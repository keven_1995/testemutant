#!/usr/bin/python
from mongoDb.mongo import Database
from patterns.Singleton import Singleton
import datetime

@Singleton
class logger:

    def __init__(self,table='table-logs',collection='collection-log'):
        self.mongo = Database().connect()
        self.mydb = self.mongo[table]
        self.myCollection = self.mydb[collection]

    def post(self,msg,activity):
        log = self.formatLog(msg,activity)
        return self.myCollection.insert_one(log)

    def getCollection(self):
        listCollection = []
        for x in self.myCollection.find():
            listCollection.append(x)
        return listCollection

    def formatLog(self,msg,activity):
        return {"activity": activity,
            "text": msg,
            "date": datetime.datetime.utcnow()}

log = logger()