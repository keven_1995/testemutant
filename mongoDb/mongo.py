#!/usr/bin/python
from pymongo import MongoClient
from pymongo import errors
from patterns.Singleton import Singleton

hostMongo = 'mongodb://localhost:27017/'
@Singleton
class Database(object):
    connection = None
    def connect(self):
        try:
            #params = config()
            client = MongoClient(hostMongo)
            return client
        except (Exception, errors.ConnectionFailure) as error:
            return error

