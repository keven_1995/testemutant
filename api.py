#!/usr/bin/python
from flask import *
from app import manipulatesRequest
from logggerMongo.logger import log

app = Flask(__name__)

@app.route("/")
def show_tables():
   data = manipulatesRequest.call()
   log.post(data.to_json(),'requisição route raiz')
   return render_template('/view.html', tables=[data.to_html(classes='display row-border hover compact',table_id='TABLE_1')],
                           titles=['na', ' '])

if __name__ == "__main__":
    app.run(port=8080,debug=True)