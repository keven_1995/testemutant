#!/usr/bin/python
import requests as req
import pandas as pd
from logggerMongo.logger import log
import json

url = 'https://jsonplaceholder.typicode.com/users'


def requestGet(url):
    return req.get(url)

def getUsers():
    res = requestGet(url)
    log.post(res.text, 'Users Sem Formato')
    return res.text


def reorganizeColunms(data):
    columnsTitles = ['name', 'email', 'website', 'company']
    return data.reindex(columns=columnsTitles)


def deleteItens(data):
    data = data.drop(columns="phone")
    data = data.drop(columns="username")
    data = data.drop(columns="id")
    data = data.drop(columns="address")
    return data


def transformCompanyName(jsonData):
    jsonData = json.loads(jsonData)
    for index,data in enumerate(jsonData):
        jsonData[index]['company'] = jsonData[index]['company']['name']
    return json.dumps(jsonData)


def onlyAndressSuite(jsonData):
    listIndex = []
    jsonData = json.loads(jsonData)
    for index,data in enumerate(jsonData):
        suite = data['address']['suite']
        suite = suite.split()
        if suite[0] != 'Suite':
            listIndex.append(index)
    for index in listIndex:
        jsonData.pop(index)
    return json.dumps(jsonData)


def transformJson(json):
    json = onlyAndressSuite(json)
    json = transformCompanyName(json)
    data = pd.read_json(json)
    data = deleteItens(data)
    data = reorganizeColunms(data)
    return data


def call():
    json = getUsers()
    return  transformJson(json)