# Developer challenge Muntant

Aplicação realiza chamada na url https://jsonplaceholder.typicode.com/users e apresenta informações seguindos os criterios:

  - Nome , Email, Website,  e empresa
  - Permite ordenação dos items 
  - Monstra todos os usuarios que contem a palavra "suite" no endereço
  - Registra logs no MongoDB

# Roadmap!

  - Testes Unitarios.
  - Aplicação e mongo no docker via docker-compose.
  - provisionamento com vagrant e virtual-box.

### Tecnologias 

Dillinger uses a number of open source projects to work properly:

* [Jinja2] - templates Html.
* [Flask] - microframework Web extensivo
* [python 3.7] - Linguagem simples e flexivel.
* [MongoDB 4.0.4] - Banco noSql para armazenar logs

### Installation

As dependências estão contidas no arquivo requirements.txt na raiz do projeto .

```sh
$ pip install -r requirements.txt
```
### Docker
Container mongodb

```sh
$  docker run -d -p 27017-27019:27017-27019 --name mongodb mongo:4.0.4
$ docker exec -it mongodb bash
```

### Detalhes

- A aplicação é iniciada em http://localhost:8080/
- O arquivo que inicia a aplicação é o api.py

### Todos

 - Escrever Testes.
 - Adicionar Comentarios Doc String.
 - adicionar Aplicação em imagem docker.

License
----

Free?!


**Free Software,Yeah!**


   [Jinja2]: <http://jinja.pocoo.org/docs/2.10/>
   [Flask]: <http://flask.pocoo.org/>
   [python 3.7]: <https://www.python.org/>
   [MongoDB 4.0.4]: <https://www.mongodb.com/>
  
  
